#!/usr/bin/env python

import sys
import pathlib
import os

from simtk.openmm.app import *
from simtk.openmm import *
from simtk.unit import *
forcefield._dataDirectories = [pathlib.Path(os.path.realpath(__file__)).parent]


import numpy as np
np.set_printoptions(suppress=True)

def bisector(point1, point2, point3):
    """
    decide local frame unit vectors x, y, z
    point1: coordinates of the atom in the origin of the local coordinate system
    point2: coordinates of first atom to define bisector
    point3: coordinates of second atom to define bisector
    """
    #create z-axis
    v1 = point2 - point1
    v1 = v1 / np.linalg.norm(v1)
    v2 = point3 - point1
    v2 = v2 / np.linalg.norm(v2)
    z = v1 + v2
    z = z / np.linalg.norm(z)

    #create x-axis by rejection   
    x = point3 - point1
    x = x - (np.dot(x,z)/np.dot(z,z))*z
    x = x / np.sqrt(np.sum(x*x))
    
    #dot = np.dot(v2, z)
    #x = v2 - dot * z
    #x = x / np.linalg.norm(x)

    #right hand rule for y
    y = np.cross(z, x) 
    return np.vstack([x, y, z]).T

def z_then_x(point1, point2, point3):
    """
    decide local frame unit vectors x, y, z
    point1: coordinates of the atom in the origin of the local coordinate system
    point2: coordinates of the atom to which the local z-axis is created
    point3: coordinates of a third atom, with which the local x-axis is created 
    """
    
    #create z-axis
    z = point2 - point1
    z = z / np.sqrt(np.sum(z*z))
    
    #create x-axis by rejection   
    x = point3 - point1
    x = x - (np.dot(x,z)/np.dot(z,z))*z
    x = x / np.sqrt(np.sum(x*x))
    
    #right hand rule for y
    y = np.cross(z, x) 
   
    #rotation matrix
    return np.vstack([x, y, z]).T


pdb = PDBFile(sys.argv[1])
forcefield = ForceField('amoebapro2013.xml')
system = forcefield.createSystem(pdb.topology)
axes = []
        

atoms = list(pdb.topology.atoms())
bonds = list(pdb.topology.bonds())
elements = [atom.element for atom in atoms]

integrator = openmm.VerletIntegrator(0.001*femtosecond)
context = Context(system, integrator)   
context.setPositions(pdb.positions)
force = system.getForces()[-3]
dipoles = force.getLabFramePermanentDipoles(context)     


#enum ParticleAxisTypes { ZThenX = 0, Bisector = 1, ZBisect = 2, ThreeFold = 3, ZOnly = 4, NoAxisType = 5 };
for atom in atoms:
    point1 = None
    point2 = None
    point3 = None

    axis = []
    pgSet = [atom.index]
    extra = []
    for x in pgSet:
        for bond in bonds:
            if bond.atom1.index == x or bond.atom2.index == x:
                extra.append(bond.atom1.index)
                extra.append(bond.atom2.index)
    pgSet = pgSet + extra
    for x in pgSet:
        for bond in bonds:
            if bond.atom1.index == x or bond.atom2.index == x:
                extra.append(bond.atom1.index)
                extra.append(bond.atom2.index)
    pgSet = pgSet + extra
    if atom.multipoleDict['axisType'] == 0:
        # z-then-x
        point1 = np.array([*pdb.positions[atom.index].value_in_unit(angstrom)])
        for other_atom in [atoms[i] for i in pgSet]:
            if int(other_atom.multipoleDict['classIndex']) == atom.multipoleDict['kIndices'][1]:
                point2 = np.array([*pdb.positions[other_atom.index].value_in_unit(angstrom)])
                if np.linalg.norm(point1-point2) > 0:
                    l2 = other_atom.name + ':(' + other_atom.multipoleDict['classIndex'] + ')' + str(other_atom.index + 1) 
                    break
        for other_atom in [atoms[i] for i in pgSet]:
            if int(other_atom.multipoleDict['classIndex']) == atom.multipoleDict['kIndices'][2]:
                point3 = np.array([*pdb.positions[other_atom.index].value_in_unit(angstrom)])
                if np.linalg.norm(point1-point3) > 0 and np.linalg.norm(point2 - point3) > 0:
                    l3 = other_atom.name + ':(' + other_atom.multipoleDict['classIndex']  + ')' + str(other_atom.index + 1)
                    break
        axis = z_then_x(point1, point2, point3)
        sys.stderr.write(atom.name + ':' + str(atom.index + 1) +'->' + l2  + ',' + l3 + '\n')
    elif atom.multipoleDict['axisType'] == 1:
        #bisector
        point1 = np.array([*pdb.positions[atom.index].value_in_unit(angstrom)])
        for other_atom in [atoms[i] for i in pgSet]:
            if int(other_atom.multipoleDict['classIndex']) == atom.multipoleDict['kIndices'][1]:
                point2 = np.array([*pdb.positions[other_atom.index].value_in_unit(angstrom)])
                if np.linalg.norm(point1-point2) > 0:
                    pgSet.remove(other_atom.index)
                    l2 = other_atom.name + ':' + str(other_atom.index)
                    break
        for other_atom in [atoms[i] for i in pgSet]:
            if int(other_atom.multipoleDict['classIndex']) == atom.multipoleDict['kIndices'][2]:
                point3 = np.array([*pdb.positions[other_atom.index].value_in_unit(angstrom)])
                if np.linalg.norm(point1-point3) > 0:
                    l3 = other_atom.name + ':' +str(other_atom.index)
                    break
        axis = bisector(point1, point2, point3)
        #if not np.allclose(axis @ atom.multipoleDict['dipole'], dipoles[atom.index]):
        #    axis = bisector(point1, point3, point2)
        sys.stderr.write(atom.name + ':' + str(atom.index + 1) +'->' + l2  + ',' + l3 + '\n')
    else:
        raise NotImplementedError(f'Missing axis type {atom.multipoleDict["axisType"]}')

    axes.append(axis)

for i in range(len(atoms)):
    if not np.allclose(axes[i] @ atoms[i].multipoleDict['dipole'], dipoles[i]):
        pass
        #assert(np.allclose(axes[i] @ atoms[i].multipoleDict['dipole'], dipoles[i]))
print('!Total charge: {:.3f}'.format(sum([atom.multipoleDict['charge'] for atom in atoms])))
print('@COORDINATES')
print(len(atoms))
print('AA')
for element, position, atom in zip(elements, pdb.positions, atoms):
    print('{:6s} {:16.12f} {:16.12f} {:16.12f} {:12d} ! {}'.format( element.symbol, *position.value_in_unit(angstrom), atom.index+1, atom.name))

print('@MULTIPOLES')
print('ORDER 0')
print(len(atoms))
for atom in atoms:
    print('{:<12d} {:16.12f}'.format(atom.index+1, atom.multipoleDict['charge']))

# v' = R @ v
# M' = R @ M @ R.T

print('ORDER 1')
print(len(atoms))
for atom in atoms:
    dipole = np.array(atom.multipoleDict['dipole']) * 18.8973
    dipole = axes[atom.index] @ dipole 
    print('{:<12d} {:16.12f} {:16.12f} {:16.12f}'.format(atom.index+1, *dipole))

print('ORDER 2')
print(len(atoms))
for atom in atoms:
    quadrupole = np.array(atom.multipoleDict['quadrupole']) * 18.8973**2 * 2
    quadrupole = quadrupole.reshape(3,3)
    quadrupole = axes[atom.index] @ quadrupole @ axes[atom.index].T
    quadrupole = quadrupole.reshape(9)
    print('{:<12d} {:16.12f} {:16.12f} {:16.12f} {:16.12f} {:16.12f} {:16.12f}'.format(atom.index+1, quadrupole[0], quadrupole[1], quadrupole[2], quadrupole[4], quadrupole[5], quadrupole[8]))

print('@POLARIZABILITIES')
print('ORDER 1 1')
print(len(atoms))
for atom in atoms:
    polarizability = atom.multipoleDict['polarizability'] * 6748.33
    print('{:<12d} {:16.12f} {:16.12f} {:16.12f} {:16.12f} {:16.12f} {:16.12f}'.format(atom.index+1, polarizability, 0., 0., polarizability, 0., polarizability))

print('@THOLE')
print(len(atoms))
for atom in atoms:
    a = atom.multipoleDict['thole'] # a in thole damping
    pdamp = atom.multipoleDict['pdamp']  # alpha ** (1/6)
    pdamp *= 4.34709754329 # sqrt(18.8973), conversion factor from (nm)**0.5 to (au)**0.5
    print('{:<12d} {:16.12f} {:16.12f}'.format(atom.index+1, a, pdamp))

print('EXCLISTS_PERMANENT')
#maxlength = max([len(atom.polarizationGroups) for atom in atoms])
maxlength = 0
for atom in atoms:
    for g in atom.polarizationGroupSet:
        if atom.index in g:
            maxlength = max(maxlength, len(g))

print(len(atoms), maxlength + 1)
for atom in atoms:
    for g in atom.polarizationGroupSet:
        if atom.index in g:
            excluded = g
    fmt = '{:<12d} ' +  ' {:>12d}'  * maxlength
    print(fmt.format(atom.index+1, *[e+1 for e in excluded], *[0]*(maxlength - len(excluded))))
